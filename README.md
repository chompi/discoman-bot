# DiscoMan-Bot

## Description

Discord music bot able to queue up and play youtube videos over discord voice channels.

## Unlisted dependencies

- `Python2.7` This version is required for node-gyp I think?
- `FFMPEG` command line tool
- `node-gyp` command line tool
- `node.js` version 6.X.X or higher

## Installation

- Install node latest stable release, this was built with node v6.11.0
- For windows run `npm install --global --production windows-build-tools`
    - Run `npm config set python python2.7`
    - Install FFMPEG from the website and add it to your system path. EX: `C:\ffmpeg\bin`
    - Run `npm install node-gyp -g`
    - Run `npm install`

## Highlights

- Here are some of the highlighted features of the bot:
    - Supports youtube mixes.
    - Supports youtube playlists.
    - Supports youtube search.
    - Supports multi-server operation.
    - Supports per-server configuration.

## Usage

To get started open up ```config.json``` and add your bot token between the tick marks ```'...'``` where it says ```'<BOT-TOKEN>'```, which you can currently aquire from registering an app at [https://discordapp.com/developers](https://discordapp.com/developers)

Run ```main.js``` with start.bat or running ```node main``` in command line after you have configured and added your token for the bot to use.

You can modify other config settings by adding the property and sub properties you want to modify to the object in ```config.json```.

There are per-server settings as well, to modify them simply run the command ```config help```.

If your having errors, try looking into the ```discoman-bot.log``` file to find the source of the cause.
