const DiscoManBot = require('./src/discoman-bot.js');
const logger = require('./src/logger.js');
const config = require('./config.json');

const bot = new DiscoManBot(config);

bot.connect()
    .then(() => { 
        bot.listen();
    })
    .catch(err => {
        logger.error(err);
    });