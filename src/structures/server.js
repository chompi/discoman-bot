const fs = require('fs');
const Queue = require('./queue.js');
const jsonfile = require('jsonfile');
const youtube = require('ytdl-core');
const moment = require('moment');

/**
 * Represents any Guild on Discord
 */
class Server {
    
    constructor(bot, id) {
        this.id = id || 0;
        this.bot = bot;
        this.queue = new Queue();
        this.dispatcher = null;
        this.textChannel = null;
        this.speakers = [];
        this.selection = [];
        this.lastTrack = null;
        this.seekTime = 0;
        this.playing = false;
        this.paused = false;
        this.playingTitle = null;
        this.leaveTimer = null;
        
        this.config = {
            auto: {
                deafen: this.bot.config.auto.deafen  
            },
            command: {
                symbol: this.bot.config.command.symbol,
                bindToChannel: this.bot.config.command.bindToChannel
            },
            search: {
                maxResults: this.bot.config.youtube.maxResults  
            },
            messages: {
                limitPerChannel: this.bot.config.messages.limitPerChannel
            },
            queue: {
                announce: this.bot.config.queue.announce,
                repeat: this.bot.config.queue.repeat
            },
            volume: this.bot.config.stream.volume
            
        };
        this.loadConfig();
    }
    
    getConfigPath() {
        return '{0}/configs/{1}.json'.format(this.bot.appDir, this.id);
    }
    
    getConfig() {
        return this.config;
    }
    
    loadConfig() {
        if (fs.existsSync(this.getConfigPath())) {
            var config = jsonfile.readFileSync(this.getConfigPath());
            if (config)
                this.config = config;
        } else {
            this.saveConfig();
        }
    }
    
    saveConfig() {
        jsonfile.writeFileSync(this.getConfigPath(), this.config);
    }
    
    getVolume() {
        return this.config.volume;
    }
    
    setVolume(volume) {
        this.config.volume = volume;
        if (this.isPlaying && this.dispatcher)
            this.dispatcher.setVolume(volume);
        this.saveConfig();
    }
    
    getRepeat() {
        return this.config.queue.repeat;
    }
    
    setRepeat(repeat) {
        this.config.queue.repeat = repeat;
        this.saveConfig();
    }
    
    getCommandSymbol() {
        return this.config.command.symbol || this.bot.config.command.symbol;
    }
    
    setSelection(tracks) {
        this.selection = tracks;
    }
    
    getSelection() {
        return this.selection;
    }
    
    setLastTrack(track) {
        this.lastTrack = track;
    }
    
    getLastTrack() {
        return this.lastTrack;
    }
    
    setTextChannel(textChannel) {
        this.textChannel = textChannel;
    }
    
    getTextChannel() {
        return this.textChannel;
    }
    
    getVoiceChannel() {
        var guild = this.bot.client.guilds.get(this.id);
        return (guild.voiceConnection ? guild.voiceConnection.channel : null);
    }
    
    isPlaying() {
        return this.playing;
    }
    
    isPaused() {
        return this.paused;
    }
    
    getPlaying() {
        return this.playingTitle;
    }
    
    play(track, opts) {
        const options = opts || {};
        const seek = options.seek || 0;
        const announce = (typeof options.announce != 'undefined' ? options.announce : true);
        if (track) {
            var voiceConnection = this.textChannel.guild.voiceConnection;
            if (voiceConnection) {
                this.playing = true;
                this.paused = false;
                this.lastTrack = track;
                this.seekTime = seek;
                try {
                    const url = track.search.contains('://') ? track.search : 'https://www.youtube.com/watch?v=' + track.search;
                    //var stream = youtube(url, { filter: (track.livestream ? this.livestreamFilter : 'audioonly'), quality: 'highestaudio', highWaterMark: 1024 * 1024 * 32 });
                    var stream = youtube(url, { quality: 'highestaudio', highWaterMark: 1024 * 1024 * 32 });

                    if (track.livestream) {
                        stream.filter = this.livestreamFilter;
                    }

                    this.dispatcher = voiceConnection.playStream(stream, { seek: seek, passes: this.bot.config.stream.passes, volume: this.getVolume() });
                    this.dispatcher.on('start', () => {
                        this.onStreamStart(track, announce);
                        voiceConnection.player.streamingData.pausedTime = 0;
                    });
                    this.dispatcher.on('end', (reason) => {
                        /*if (reason == undefined) {
                            this.onSteamEndedSudden();
                            return;
                        }
                        if (reason == 'Stream is not generating quickly enough.') {
                            this.onSteamEndedSudden();
                            return;
                        }*/
                        this.onStreamEnded();
                    });
                    this.dispatcher.on('error', (err) => {
                        this.onStreamError(err);
                    });
                }
                catch (e) {
                    console.log(e);
                }
            }
        }
    }
    
    playStation(station) {
        if (station) {
            var voiceConnection = this.textChannel.guild.voiceConnection;
            if (voiceConnection) {
                try {
                    this.playing = true;
                    this.paused = false;
                    
                    this.dispatcher = voiceConnection.playStream(station.url, { seek: 0, passes: this.bot.config.stream.passes, volume: this.getVolume() });
                    this.dispatcher.on('start', () => {
                        this.playingTitle = station.name;
                        this.textChannel.send(`:musical_note: Now playing radio station ${station.name}.`);
                        voiceConnection.player.streamingData.pausedTime = 0;
                    });
                    this.dispatcher.on('end', () => {
                        this.dispatcher = null;
                        this.playing = false;
                        this.paused = false;
                    });
                    this.dispatcher.on('error', (err) => {
                        if (this.dispatcher) {
                            this.dispatcher.end();
                        }
                    });
                }
                catch (e) {
                    console.log(e);
                }
            }
        }
    }
    
    livestreamFilter(format) {
        var itags = [91, 92, 93, /*94, 95,*/ 127, 128];
        if (itags.indexOf(parseInt(format.itag)) > -1) {
            return true;
        }
        return false;
    }
    
    onStreamStart(track, announce) {
        if (!announce || !this.config.queue.announce || !track.title)
            return;
        this.playingTitle = track.title;
        if (track.requestor !== undefined) {
            this.textChannel.send(`:musical_note: Now playing: "${track.title}" (${track.duration}), Requested by: ${track.requestor}.`);
        } else {
            this.textChannel.send(`:musical_note: Now playing: "${track.title}" (${track.duration}).`);
        }
    }
    
    onStreamEnded() {
        this.dispatcher = null;
        if (this.queue.first && this.isPlaying()) {
            this.playing = false;
            this.paused = false;
            var currentTrack = this.queue.dequeue();
            if (this.getRepeat())
                this.queue.enqueue(currentTrack);
            var nextTrack = this.queue.first;
            if (nextTrack)
                this.play(nextTrack);
        }
    }
    
    onStreamError(err) {
        var currentTrack = this.queue.first;
        if (currentTrack && this.dispatcher) {
            this.dispatcher.end();
            this.textChannel.send(`:fast_forward: "${currentTrack.title}" skipped because of an error.`);
        }
        return this.textChannel.send('Error: ' + err);
    }

    onSteamEndedSudden() {
        this.dispatcher = null;
        if (this.queue.first && this.isPlaying()) {
            this.playing = false;
            this.paused = false;
            var currentTrack = this.queue.first;
            this.play(currentTrack, { seek: this.time() });
        }
    }
    
    playFirst(opts) {
        this.play(this.queue.first, opts);
    }
    
    stop() {
        if (this.isPlaying() && this.dispatcher) {
            this.playing = false;
            this.paused = false;
            this.dispatcher.end();
            return true;
        }
        return false;
    }
    
    pause() {
        if (this.isPlaying() && this.dispatcher) {
            this.paused = true;
            this.dispatcher.pause();
            return true;
        }
        return false;
    }
    
    resume() {
        if (this.isPaused() && this.dispatcher) {
            this.paused = false;
            this.dispatcher.resume();
            return true;
        }
        return false;
    }
    
    replay() {
        if (this.isPlaying() && this.dispatcher) {
            this.playing = false;
            this.paused = false;
            this.dispatcher.end();
        }
        var lastTrack = this.getLastTrack();
        if (lastTrack) {
            this.play(lastTrack);
        }
    }
    
    skip() {
        var track = this.queue.first;
        if (track && this.dispatcher) {
            this.playing = true;
            this.dispatcher.end();
            return true;
        }
        return false;
    }
    
    seek(seek) {
        var track = this.queue.first;
        if (track) {
            this.stop();
            this.play(track, { seek: seek, announce: false });
        }
    }
    
    time() {
        if (this.queue.first && this.dispatcher) {
            return this.seekTime + (this.dispatcher.time / 1000);
        }
        return 0;
    }
    
    enqueue(track, announce) {
        if (announce == undefined)
            announce = true;
        var $server = this;
        return new Promise((resolve, reject) => {
            $server.queue.enqueue(track);
            var url = track.search.contains('://') ? track.search : 'https://www.youtube.com/watch?v=' + track.search;
            youtube.getInfo(url, (err, info) => {
                if (err) {
                    $server.queue.removeTrack(track);
                } else {
                    track.title = info.title;
                    track.livestream = (info.livestream == undefined) ? false : true;
                    track.length = moment('00:00:00', 'HH:mm:ss').add(parseInt(info.length_seconds), 's').format('HH:mm:ss');
                    track.duration = (track.livestream ? 'Live' : track.length);
                    if ($server.isPlaying()) {
                        if (announce)
                            $server.textChannel.send(`:heavy_plus_sign: Youtube Enqueued: "${track.title}" (${track.duration}) @ #${$server.queue.indexOf(track) + 1}`);
                    } else {
                        $server.playFirst();
                    }
                }
                resolve();
            });
        });
    }
        
    startLeaveTimer() {
        if (this.leaveTimer)
            clearTimeout(this.leaveTimer);
        
        var $server = this;
        
        this.leaveTimer = setTimeout(function() {
            // Get the bot voice channel
            var clientVoiceChannel = $server.getVoiceChannel();
            
            // Check if the channel is still empty
            if (clientVoiceChannel && clientVoiceChannel.members.array().length <= 1) {
                clientVoiceChannel.connection.disconnect();
            }
            
            // Remove the timeout object
            $server.leaveTimer = null;
        }, this.bot.config.auto.leaveTime);
    }
    
    stopLeaveTimer() {
        if (this.leaveTimer)
            clearTimeout(this.leaveTimer);
    }
}

module.exports = Server;
