const fs = require('fs');
const jsonfile = require('jsonfile');
const moment = require('moment');

/**
 * Represents any Guild on Discord
 */
class Radio {
    
    constructor(bot) {
        this.bot = bot;
        this.stations = [];
        
        this.loadStations();
    }
    
    getFilePath() {
        return '{0}/configs/radios.json'.format(this.bot.appDir);
    }
    
    loadStations() {
        if (fs.existsSync(this.getFilePath())) {
            var stations = jsonfile.readFileSync(this.getFilePath());
            if (stations)
                this.stations = stations;
        } else {
            this.saveStations();
        }
    }
    
    saveStations() {
        jsonfile.writeFileSync(this.getFilePath(), this.stations);
    }
    
    getStations() {
        return this.stations;
    }
    
    getStationByIndex(index) {
        return this.stations[index];
    }
    
    getStationByName(name) {
        for (var i = 0; i < this.stations.length; i++) {
            if (this.stations[i].name.toLowerCase() == name.toLowerCase())
                return this.stations[i];
        }
        return null;
    }
    
    addStation(name, url) {
        var station = { name: name, url: url };
        this.stations.push(station);
        this.saveStations();
    }
}

module.exports = Radio;