var querystring = require('querystring');
var xhr = require('xhr');

if (!xhr.open) xhr = require('request');

var allowedProperties = [
    'fields',
    'id',
    'onBehalfOfContentOwner',
    'pageToken',
    'videoId',
    'key'
];

module.exports = function(term, opts, cb) {
    if (typeof opts === 'function') {
        cb = opts;
        opts = {};
    }

    var params = {
        playlistId: term,
        part: opts.part || 'id,snippet,contentDetails',
        maxResults: opts.maxResults || 30
    };

    Object.keys(opts).map(function (k) {
        if (allowedProperties.indexOf(k) > -1) params[k] = opts[k];
    });

    xhr({
        url: 'https://www.googleapis.com/youtube/v3/playlistItems?' + querystring.stringify(params),
        method: 'GET'
    },
    function (err, res, body) {
        if (err) return cb(err);

        try {
            var result = JSON.parse(body);

            if (result.error) {
                var error = new Error(result.error.errors.shift().message);
                return cb(error);
            }

            var pageInfo = {
                totalResults: result.pageInfo.totalResults,
                resultsPerPage: result.pageInfo.resultsPerPage,
                nextPageToken: result.nextPageToken,
                prevPageToken: result.prevPageToken
            };

            return cb(null, result.items, pageInfo);
        } catch(e) {
            return cb(e);
        }
    });
};