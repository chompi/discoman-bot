const __ = require('iterate-js');

module.exports = function(bot, config) {
    bot.config = new __.lib.Config({
        auto: new __.lib.Config({
            //voicechannel: "390850900337623053",
            //textchannel: "390855178141696000",
            deafen: false,
            reconnect: true,
            leave: true,
            leaveTime: 60000 // 1 minute
        }),
        command: new __.lib.Config({
            symbol: '!',
            bindToChannel: null
        }),
        discord: new __.lib.Config({
            token: '<BOT-TOKEN>',
            log: true
        }),
        youtube: new __.lib.Config({
            maxResults: 10,
            key: '<API-KEY>'
        }),
        messages: new __.lib.Config({
            limitPerChannel: 10
        }),
        queue: new __.lib.Config({
            announce: true,
            repeat: false
        }),
        stream: new __.lib.Config({
            passes: 2, //can be increased to reduce packetloss at the expense of upload bandwidth, 4-5 should be lossless at the expense of 4-5x upload
            volume: 0.1
        })
    });
    bot.config.update(config);
};