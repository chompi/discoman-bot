module.exports = [
    '{0}config auto deafen: "Enables or disables auto deafen of the bot"',
    '{0}config command symbol [symbol]: "Sets the command symbol"',
    '{0}config command bind: "Binds to bot commands to the current channel"',
    '{0}config command unbind: "Unbinds to bot commands from any channel"',
    '{0}config search maxresults [number]: "Sets the max search results"',
    '{0}config messages limit [number]: "Sets the message limit per channel"',
    '{0}config queue announce: "Enables or disables the queue announcements"'
].join('\n');
