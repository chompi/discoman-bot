const __            = require('iterate-js');
const moment        = require('moment');
const logger        = require('../logger.js');
const helpText      = require('../helptext.js');
const helpTextCfg   = require('../helptext-config.js');
const Server        = require('../structures/server.js');
const Queue         = require('../structures/queue.js');
const youtube       = require('ytdl-core');
const ytpl          = require('ytpl');
const ytsearch      = require('youtube-search');
const ytPlaylistId  = require('get-youtube-playlist-id')
const ytmix         = require('../youtube-mix.js');
const util          = require('../utils');

module.exports = function(bot) {
    
    var sequentialEnqueue = (msg, tracks, ann, cb) => {
        var track = tracks.shift();
        if (track) {
            msg.server.enqueue(track, ann)
                .then(() => { sequentialEnqueue(msg, tracks, ann, cb); })
                .catch(error => logger.error(error));
        } else if(cb) cb();
    };
    
    bot.commands = {

        help: msg => {
            msg.channel.send('Help Menu :question:\n```' + helpText.format(msg.server.getCommandSymbol()) + '```', { split: true });
        },

        ping: msg => {
            msg.channel.send(`:ping_pong: Pong!`);
        },

        join: msg => {
            return new Promise((resolve, reject) => {
                var voicechannel = msg.member.voiceChannel;
                if (voicechannel && voicechannel.type == 'voice') {
                    // Join the voice channel
                    voicechannel.join()
                        .then(connection => {
                            resolve(connection);
                            connection.on('error', (error) => {
                                logger.error(error);
                            });
                            /*connection.on('debug', (message) => {
                                console.log(`Debug: ${message}`);
                            });*/
                        }).catch(err => reject(err));
                } else
                    return msg.channel.send("I couldn't connect to your voice channel.");
            });
        },

        summon: msg => {
            return bot.commands.join(msg);  
        },
        
        leave: msg => {
            bot.commands.stop(msg);
            if (msg.guild.voiceConnection) {
                msg.guild.voiceConnection.disconnect();
            }
        },
        
        p: msg => {
            return bot.commands.play(msg);  
        },
        
        play: msg => {
            var params = msg.details.trim();
            
            if (!msg.guild.voiceConnection)
                return bot.commands.join(msg).then(() => bot.commands.play(msg));
            
            if (params.length > 0) {
                bot.commands.youtube(msg);
            } else {
                if (msg.server.queue.count == 0)
                    return msg.channel.send(msg.trans ? 'Add some songs to the queue first.' : 'No remaining songs in the queue.');
                
                if (msg.server.isPlaying())
                    return msg.channel.send('Already playing a song.');

                msg.server.playFirst();
            }
        },
        
        stop: msg => {
            if (msg.server.getPlaying() && msg.server.stop() && msg.channel) {
                msg.channel.send(`:stop_button: "${msg.server.getPlaying()}" stopped.`);
            }
        },
        
        pause: msg => {
            if (msg.server.getPlaying() && msg.server.pause() && msg.channel) {
                msg.channel.send(`:pause_button: "${msg.server.getPlaying()}" paused.`);
            }
        },

        resume: msg => {
            if (msg.server.getPlaying() && msg.server.resume() && msg.channel) {
                msg.channel.send(`:play_pause: "${msg.server.getPlaying()}" resumed.`);
            }
        },
        
        replay: msg => {
            msg.server.replay();
        },
        
        skip: msg => {
            var track = msg.server.queue.first;
            if (track && msg.server.skip() && msg.channel) {
                msg.channel.send(`:fast_forward: "${track.title}" skipped.`);
            }
        },

        time: msg => {
            var track = msg.server.queue.first;
            if (track) {
                msg.channel.send(':clock2: time: {0} / {1}'.format(moment('00:00:00', 'HH:mm:ss').add(msg.server.time(), 's').format('HH:mm:ss'), track.length));
            }
        },

        seek: msg => {
            var params = msg.details.trim();
            
            if (params.length == 0)
                return;
            
            var regex = new RegExp('^[0-9:]+$');
            if (!regex.test(params))
            {
                msg.channel.send('Invalid time parameter.');
                return;
            }
            
            var seek = 0;
            if (params.contains(':')) {
                seek = params.split(':').reverse().reduce((prev, curr, i) => prev + curr * Math.pow(60, i), 0);
            } else {
                seek = parseInt(params);
            }
            
            msg.server.seek(seek);
        },
        
        youtube: msg => {
            var search = msg.details.trim();
            
            if (search.length == 0)
                return;
            
            if (!msg.guild.voiceConnection)
                return bot.commands.join(msg).then(() => bot.commands.youtube(msg));
            
            // Handle URLs
            if (search.contains('://') && search.contains('youtube.com')) {
                if (search.contains('youtube.com/playlist')) {
                    // Handle playlist
                    ytpl(search, (err, playlist) => {
                        if (playlist) {
                            msg.channel.send(`:heavy_plus_sign: Processing ${playlist.total_items} tracks from the playlist "${playlist.title}".`);
                            __.all(playlist.items, item => {
                                msg.server.enqueue({ title: item.title, search: item.url, requestor: msg.author.username }, false);
                            });
                        }
                    });
                } else {
                    // Handle single track
                    msg.server.enqueue({ title: null, search: search, requestor: msg.author.username });
                }
            } else {
                msg.playFirst = true;
                // Handle search query
                bot.commands.search(msg);
            }
        },
        
        search: msg => {
            var search = msg.details.trim();
            
            if (search.length == 0)
                return;
            
            if (!msg.guild.voiceConnection)
                return bot.commands.join(msg).then(() => bot.commands.search(msg));
            
            if (bot.config.youtube.key == '<API-KEY>') {
                msg.channel.send('Youtube API key not configured.');
                return;
            }
            
            var options = {
                key: bot.config.youtube.key,
                maxResults: msg.server.config.search.maxResults,
                type: 'video'
            };
            
            ytsearch(search, options, function(err, results) {
                if (err)
                    return console.log(err);
                
                // Handle results
                if (results && results.length > 0) {
                    if (results.length == 1 || msg.playFirst) {
                        // Handle single result
                        var result = results[0];
                        msg.server.enqueue({ title: result.title, search: result.link, requestor: msg.author.username });
                    } else {
                        // Handle multiple results
                        var tracks = [];
                        var index = 0;
                        var message = 'Search results\n```';
                        __.all(results, result => {
                            index++;
                            tracks.push({ title: result.title, search: result.link, requestor: msg.author.username });
                            message += `${index}. ${result.title}` + '\n';
                        });
                        msg.server.setSelection(tracks);
                        message += '```\n';
                        message += `Enter \`${msg.server.getCommandSymbol()}select <number|first-last>\` to continue.`;
                        msg.channel.send(message, { split: true });
                    }
                } else {
                    msg.channel.send('Dang it, could not find anything.');
                }
            });
        },
        
        select: msg => {
            var param = msg.details;
            var tracks = msg.server.getSelection();
            
            if (tracks.length == 0) {
                msg.channel.send('Please do a search first.');
                return;
            }
            
            var regex = new RegExp('^[0-9- ]+$');
            if (!regex.test(param))
            {
                msg.channel.send('Invalid selection.');
                return;
            }
            
            var indexes = [];
            if (param.contains(' ')) {
                indexes = param.split(' ');
            } else if (param.contains('-')) {
                var range = param.split('-');
                var min = parseInt(range[0]);
                var max = parseInt(range[1]);
                for (var a = min; a <= max; a++) {
                    indexes.push(a);
                }
            } else {
                indexes.push(param);
            }
            
            var enqueuTracks = [];
            for (var i = 0; i < indexes.length; i++) {
                var index = parseInt(indexes[i] - 1);
                if (index >= tracks.length) {
                    continue;
                }
                if (tracks[index] !== undefined) {
                    enqueuTracks.push(tracks[index]);
                }
            }
            sequentialEnqueue(msg, enqueuTracks);
        },
        
        s: msg => {
            bot.commands.select(msg);  
        },
        
        mix: msg => {
            var search = msg.details.trim();
            
            if (search.length == 0)
                return;
            
            if (!msg.guild.voiceConnection)
                return bot.commands.join(msg).then(() => bot.commands.mix(msg));
            
            if (bot.config.youtube.key == '<API-KEY>') {
                msg.channel.send('Youtube API key not configured.');
                return;
            }
            
            var playlistId = ytPlaylistId(search);
            if (playlistId !== false) {
                var options = {
                    key: bot.config.youtube.key,
                    maxResults: 50
                };
                ytmix(playlistId, options, (err, results) => {
                    if (err) return console.log(err);
                    if (results && results.length > 0) {
                        msg.channel.send(`:heavy_plus_sign: Processing ${results.length} tracks.`);
                        __.all(results, item => {
                            msg.server.enqueue({
                                title: item.snippet.title, 
                                search: 'https://www.youtube.com/watch?v=' + item.contentDetails.videoId, 
                                requestor: msg.author.username
                            }, false);
                        });
                    }
                });
            } else {
                msg.channel.send('Invalid youtube URL.');
            }
        },
        
        yt: msg => {
            bot.commands.youtube(msg);
        },
        
        remove: msg => {
            bot.commands.dequeue(msg);
        },

        dequeue: msg => {
            var songidx = msg.details.trim();
            if (songidx != '') {
                songidx = parseInt(songidx) - 1;
                if (songidx == 0) {
                    bot.commands.stop(msg);
                }
                var track = msg.server.queue.at(songidx);
                msg.channel.send(`:heavy_minus_sign: Dequeued: ${track.title}`);
                msg.server.queue.remove((track, idx) => idx == songidx);
            }
        },

        list: msg => {
            var list = __.map(msg.server.queue.list, (track, idx) => `${idx + 1}. "${track.title}${track.requestor ? ` Requested By: ${track.requestor}`:''}"`);
            if (list.length > 0)
                msg.channel.send('Current queue\n\n' + list.join('\n'), { split: true });
            else
                msg.channel.send(':cd: There are no songs in the queue.');
        },

        clear: msg => {
            bot.commands.stop(msg);
            msg.server.queue.clear();
            msg.channel.send(':cd: Playlist Cleared.');
        },

        move: msg => {
            var parts = msg.details.split(' '),
                current = parts[0],
                target = parts[1];
            
            if (current && current != '' && target != '') {
                current = parseInt(current) - 1;
                var track = msg.server.queue.at(current);
                var regex = new RegExp('^[0-9 ]+$');
                
                if (target.contains('up', true)) {
                    target =  current - 1;
                } else if (target.contains('down', true)) {
                    target = current + 1;
                } else if (regex.test(target)) {
                    target = parseInt(target) - 1;
                }
                
                if (target >= 0 && target <= msg.server.queue.count - 1) {
                    if (current == 0 || target == 0)
                        bot.commands.stop(msg);
                    msg.server.queue.move(current, target);
                    msg.channel.send(`:arrow_${target > current ? 'down' : 'up'}: Track: ${track.title} Moved to #${target + 1}`);
                }
            }
        },

        shuffle: msg => {
            bot.commands.stop(msg);
            msg.server.queue.shuffle();
            msg.channel.send(':arrows_counterclockwise: Queue Shuffled.');
        },

        volume: msg => {
            var volume = msg.details.trim();
            if (volume != '') {
                volume = __.math.between(parseInt(volume), 0, 100);
                volume = (volume / 100);
                msg.server.setVolume(volume);
                msg.channel.send(`:speaker: Volume set to ${volume * 100}%`);
            } else
                msg.channel.send(`:speaker: Volume set to ${msg.server.getVolume() * 100}%`);
        },

        repeat: msg => {
            msg.server.setRepeat(!msg.server.getRepeat());
            msg.channel.send(`Repeat mode is ${msg.server.getRepeat() ? 'on' : 'off'}`);
        },

        playlist: msg => {
            var parts = msg.details.split(' '),
                action = parts[0].toLowerCase(),
                parameter = parts[1];
            
            __.switch(action, {
                save: () => {
                    if (parameter != undefined) {
                        bot.playlist.save(msg.server, parameter);
                        msg.channel.send(`Playlist: "${parameter}" has been saved.`);
                    }
                },
                load: () => {
                    if (parameter != undefined) {
                        bot.commands.stop(msg);
                        bot.playlist.load(msg.server, parameter);
                        msg.channel.send(`Playlist: "${parameter}" has been loaded.`);
                    }
                },
                delete: () => {
                    if (parameter != undefined) {
                        bot.playlist.delete(parameter);
                        msg.channel.send(`Playlist: "${parameter}" has been deleted.`);
                    }
                },
                list: () => {
                    var playlists = bot.playlist.list();
                    if (playlists.length > 0) {
                        playlists = __.map(playlists, (x, y) => '{0}. {1}'.format(y + 1, x));
                        msg.channel.send('Available playlist\n```' + playlists.join('\n') + '```', { split: true });
                    } else {
                        msg.channel.send('There are no saved playlists.');
                    }
                }
            });
        },
        
        radio: msg => {
            var sepIndex = msg.details.indexOf(' ');
            var action = msg.details.substr(0, (sepIndex > -1) ? sepIndex : msg.details.length);
            var parameters = (sepIndex > -1) ? msg.details.substr(sepIndex + 1).trim() : '';
            
            if (action.length == 0)
                return;
            
            __.switch(action, {
                add: () => {
                    if (parameters == '')
                        return;
                    
                    var name, url;
                    
                    if (parameters.substr(0, 1) == '"') {
                        parameters = parameters.substr(1);
                        name = parameters.substr(0, parameters.indexOf('"'));
                        parameters = parameters.substr(parameters.indexOf('"') + 1);
                        url = parameters.substr(1);
                    } else {
                        var parts = parameters.split(' ');
                        name = parts[0];
                        url = parts[1];
                    }
                    
                    bot.radio.addStation(name, url);
                    msg.channel.send(`Radio station ${name} added.`);
                },
                play: () => {
                    if (parameters == '')
                        return;
                    
                    if (!msg.guild.voiceConnection)
                        return bot.commands.join(msg).then(() => bot.commands.radio(msg));
                    
                    var selection = parameters;
                    var station;
                    
                    var regex = new RegExp('^[0-9]+$');
                    if (regex.test(selection)) {
                        station = bot.radio.getStationByIndex(parseInt(selection) - 1);
                    } else {
                        station = bot.radio.getStationByName(selection);
                    }
                    
                    if (station != undefined && station != null) {
                        if (msg.server.isPlaying())
                            bot.commands.stop(msg);
                        
                        msg.server.playStation(station);
                    } else {
                        msg.channel.send(`Invalid station selection.`);
                    }
                },
                list: () => {
                    var stations = bot.radio.getStations();
                    if (stations.length > 0) {
                        stations = __.map(stations, (x, y) => '{0}. {1}'.format(y + 1, x.name));
                        msg.channel.send('Available radio stations\n```' + stations.join('\n') + '```', { split: true });
                    } else {
                        msg.channel.send(`There are no radio stations available.`);
                    }
                }
            });
        },
        
        config: msg => {
            var group = msg.details.substr(0, msg.details.indexOf(' '));
            var parameters = msg.details.substr(msg.details.indexOf(' ') + 1).split(' ');
            var setting = parameters[0].toLowerCase();
            var value = parameters[1];
            
            // Check for admin permission
            if (!msg.member || !msg.member.hasPermission('ADMINISTRATOR')) {
                msg.channel.send(`You don't have permissions to do that.`);
                return;
            }
            
            __.switch(group, {
                auto: () => {
                    if (setting == 'deafen') {
                        msg.server.config.auto.deafen = !msg.server.config.auto.deafen;
                        msg.server.saveConfig();
                        if (msg.server.config.auto.deafen) {
                            msg.channel.send(`Auto deafen is now Enabled.`);
                        } else {
                            msg.channel.send(`Auto deafen is now Disabled.`);
                        }
                    }
                },
                command: () => {
                    __.switch(setting, {
                        symbol: () => {
                            if (value != undefined) {
                                msg.server.config.command.symbol = value;
                                msg.server.saveConfig();
                                msg.channel.send(`Command symbol has been set to "${value}".`);
                            }
                        },
                        bind: () => {
                            msg.server.config.command.bindToChannel = msg.channel.id;
                            msg.server.saveConfig();
                            msg.channel.send(`Commands are now bound to channel "${msg.channel.name}".`);
                        },
                        unbind: () => {
                            msg.server.config.command.bindToChannel = null;
                            msg.server.saveConfig();
                            msg.channel.send(`Commands are no longer bound to a channel.`);
                        }
                    });
                },
                search: () => {
                    if (setting == 'maxresults' && value != undefined) {
                        var regex = new RegExp('^[0-9]+$');
                        if (!regex.test(value))
                        {
                            msg.channel.send('Invalid value, please use digits only.');
                            return;
                        }
                        msg.server.config.search.maxResults = parseInt(value);
                        msg.server.saveConfig();
                        msg.channel.send(`Search max results has been set to "${value}".`);
                    }
                },
                messages: () => {
                    if (setting == 'limit' && value != undefined) {
                        var regex = new RegExp('^[0-9]+$');
                        if (!regex.test(value))
                        {
                            msg.channel.send('Invalid value, please use digits only.');
                            return;
                        }
                        msg.server.config.messages.limitPerChannel = parseInt(value);
                        msg.server.saveConfig();
                        msg.channel.send(`Messages limit per channel has been set to "${value}".`);
                    }
                },
                queue: () => {
                    if (setting == 'announce') {
                        msg.server.config.queue.announce = !msg.server.config.queue.announce;
                        msg.server.saveConfig();
                        if (msg.server.config.queue.announce) {
                            msg.channel.send(`Queue announce is now Enabled.`);
                        } else {
                            msg.channel.send(`Queue announce is now Disabled.`);
                        }
                    }
                }
            });
            
            // Handle help
            if (msg.details.trim().toLowerCase() == 'help') {
                msg.channel.send('Config Help Menu :question:\n```' + helpTextCfg.format(msg.server.getCommandSymbol()) + '```', { split: true });
            }
        }
    };
};