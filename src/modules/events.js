const __ = require('iterate-js');
const logger = require('../logger.js');

module.exports = function(bot) {

    var parseMsg = (msg) => {
        msg.meta = msg.content.split(' ');
        var x = msg.meta.slice();
        msg.cmd = x.shift().replace(msg.server.getCommandSymbol(), '');
        msg.details = x.join(' ');
        return msg;
    };

    __.all({
        message: msg => {
            // Cannot operate outside of a guild
            if (!msg.guild)
                return;
            
            // Messages by users
            if (msg.author.id != bot.client.user.id) {
                // Get the server object
                msg.server = bot.getServer(msg.guild.id);

                // Check if the commands are bound to a channel
                if (msg.server.config.command.bindToChannel && msg.channel.id != msg.server.config.command.bindToChannel)
                    return;

                var commandSymbol = msg.server.getCommandSymbol();
                var hasCommand = (content) => content.substring(0, commandSymbol.length) == commandSymbol;

                if (msg.content && hasCommand(msg.content)) {
                    // Set the text channel on the server object
                    msg.server.setTextChannel(msg.channel);

                    if (bot.config.discord.log) {
                        logger.log('{0}{1}{2} : {3}'.format(
                            msg.guild ? '{0} '.format(msg.guild.name) : '', 
                            msg.channel.name ? '#{0} @ '.format(msg.channel.name) : 'PM @ ', 
                            msg.author.username, 
                            msg.content
                        ));
                    }

                    try {
                        var data = parseMsg(msg),
                            cmd = bot.commands[data.cmd];
                        if (__.is.function(cmd))
                            cmd(data);
                    } catch(e) {
                        logger.error(e);
                    }
                }
            } else {
                // Clean bot messages
                try {
                    bot.manager.cleanChannel(msg.channel);
                } catch(e) {
                    logger.error(e);
                }
            }
        },

        ready: () => {
            if (bot.online)
                logger.log('Reconnected.');
            else
                logger.log('DiscoMan Online.');
            bot.online = true;
            // Development stuff
            if (bot.config.auto.voicechannel != undefined && bot.config.auto.textchannel != undefined) {
                var voicechannel = bot.client.channels.get(bot.config.auto.voicechannel);
                var textchannel = bot.client.channels.get(bot.config.auto.textchannel);
                if (voicechannel && textchannel) {
                    voicechannel.join().then(connection => {
                        var server = bot.getServer(voicechannel.guild.id);
                        server.setTextChannel(textchannel);
                        server.speakers = [];
                        connection.on('error', (error) => {
                            logger.error(error);
                        });
                        console.log(`Auto connected to channel: ${voicechannel.name}.`);
                    }).catch((err) => { console.log(err); });
                }
            }
        },

        reconnecting: () => {
            logger.log('Reconnecting...');
        },

        disconnect: () => {
            bot.online = false;
            logger.log('Disconnected.');
        },

        error: error => {
            logger.error(error);
        },

        guildMemberUpdate: (old, member) => {
            if (member.user.username == bot.client.user.username && member.mute) {
                member.setMute(false);
                logger.log('DiscoMan muted....unmuteing');
            }
        },

        guildMemberSpeaking: (member, isSpeaking) => {
            var server = bot.getServer(member.guild.id);
            
            if (isSpeaking)
                server.speakers.push(member.id);
            else {
                var idx = server.speakers.indexOf(member.id);
                if (idx > -1)
                    server.speakers.splice(idx, 1);
            }

            if (server.config.auto.deafen) {
                if (server.isPlaying() && server.dispatcher) {
                    if (server.speakers.length > 0)
                        server.dispatcher.setVolume(0);
                    else
                        server.dispatcher.setVolume(server.getVolume());
                }
            }
        },
        
        voiceStateUpdate: (old, member) => {
            var server = bot.getServer(member.guild.id);
            
            // Detect when bot enters or leaves voice
            if (member.user.id == bot.client.user.id) {
                server.speakers = [];
                var textChannel = server.getTextChannel();
                if (textChannel) {
                    if (member.voiceChannel !== undefined) {
                        textChannel.send(`:speaking_head: Joined channel: ${member.voiceChannel.name}.`);
                    } else {
                        server.stop();
                        textChannel.send(`:mute: Disconnecting from channel: ${old.voiceChannel.name}.`);
                    }
                }
            } else if (bot.config.auto.leave) {
                // Get the bot voice channel
                var clientVoiceChannel = server.getVoiceChannel();
                
                if (clientVoiceChannel) {
                    // Manage auto leave
                    if (old.voiceChannel && clientVoiceChannel.id == old.voiceChannel.id) {
                        // then check if it's empty
                        if (clientVoiceChannel.members.array().length <= 1) {
                            server.startLeaveTimer();
                        }
                    }
                    if (member.voiceChannel && clientVoiceChannel.id == member.voiceChannel.id) {
                        server.stopLeaveTimer();
                    }
                }
            }
        }
        
    }, (func, name) => { bot.client.on(name, func); });
};
